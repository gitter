doc:
	$(MAKE) -C doc

clean:
	$(MAKE) -C doc clean

.PHONY: doc
