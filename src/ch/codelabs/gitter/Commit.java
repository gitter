/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

public class Commit {

	public String mTitle;
	public String mAuthor;
	public String mEmail;
	public String mDate;
	public String mDiffLink;
	public String mContent;
	public String mCommitId;

	/**
	 * Checks if all member variables have been filled with data.
	 * 
	 * @return True if all commit info is set
	 */
	public boolean isSet() {
		return (this.mTitle != null && this.mAuthor != null
				&& this.mEmail != null && this.mDate != null
				&& this.mDiffLink != null && this.mContent != null && this.mCommitId != null);
	}

	/**
	 * Trims all string values of the commit.
	 */
	public void trim() {
		mTitle = mTitle.trim();
		mAuthor = mAuthor.trim();
		mEmail = mEmail.trim();
		mDate = mDate.trim();
		mDiffLink = mDiffLink.trim();
		mContent = mContent.trim();
		mCommitId = mCommitId.trim();
	}
}
