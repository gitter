/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class PrefsActivity extends PreferenceActivity implements
		OnSharedPreferenceChangeListener {

	/* URL alert dialog identifier */
	static final int DIALOG_URL_CHANGE_ALERT = 1;

	/* Preferences */
	private SharedPreferences mPrefs;

	/* Last set repository URL */
	private String mLastRepoUrl;

	/* Result passed back to calling activity */
	private Intent mResult;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mLastRepoUrl = mPrefs.getString(
				getString(R.string.pref_repository_url), null);
		mPrefs.registerOnSharedPreferenceChangeListener(this);
		mResult = new Intent();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_URL_CHANGE_ALERT:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					getString(R.string.repository_url_change_warning))
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
									mPrefs.edit().commit();
									mLastRepoUrl = mPrefs
											.getString(
													getString(R.string.pref_repository_url),
													null);
									mResult.putExtra(
											getString(R.string.intent_pref_url_changed),
											true);
									setResult(RESULT_OK, mResult);
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									mPrefs.edit()
											.putString(
													getString(R.string.pref_repository_url),
													mLastRepoUrl).commit();
									reloadUrlText();
								}
							});
			return builder.create();
		default:
			return super.onCreateDialog(id);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		mPrefs.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		mPrefs.unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
	}

	/**
	 * Handle preference changes.
	 */
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(getString(R.string.pref_repository_url))) {
			showDialog(DIALOG_URL_CHANGE_ALERT);
		} else if (key.equals(getString(R.string.pref_service_enabled))) {
			mResult.putExtra(
					getString(R.string.intent_pref_service_state_changed), true);
			setResult(RESULT_OK, mResult);
		} else if (key.equals(getString(R.string.pref_service_update_interval))) {
			mResult.putExtra(
					getString(R.string.intent_pref_update_interval_changed),
					true);
			setResult(RESULT_OK, mResult);
		}
	}

	/**
	 * Reload text of repository URL preference.
	 */
	private void reloadUrlText() {
		EditTextPreference repoUrl = (EditTextPreference) findPreference(getString(R.string.pref_repository_url));
		repoUrl.setText(mPrefs.getString(
				getString(R.string.pref_repository_url), null));
	}
}