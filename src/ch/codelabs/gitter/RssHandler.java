/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class RssHandler extends DefaultHandler {

	/* TAG for logging purposes */
	private static final String TAG = RssHandler.class.getSimpleName();

	/* Tag of commit diff in commit diff URL */
	private static final String COMMIT_DIFF_TAG = "commitdiff";

	/* Commit ID (SHA1 hash) length (# of characters) */
	private static final int COMMIT_ID_LENGTH = 40;

	/* Length of abbreviated commit hash */
	private static final int COMMIT_ABBREV_LENGTH = 7;

	/* Commit database instance */
	private final CommitDbAdapter mCommitDb;

	/* RSS Parser states */
	private enum RssState {
		CHANNEL, ITEM, ITEM_TITLE, ITEM_AUTHOR, ITEM_DATE, ITEM_LINK, ITEM_CONTENT
	}

	/* Date formatter */
	private final SimpleDateFormat mDateFormatter = new SimpleDateFormat(
			"EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

	/* Current parser state */
	private RssState mCurState;

	/* Current commit that is being processed */
	private Commit mCurCommit;

	/* String builder used to build RSS element data */
	private StringBuilder mRssData;

	/* List of commits to insert into database */
	List<Commit> mCommitList = new ArrayList<Commit>();

	/* Commit ID of last fetched commit */
	private String mLastCommitId;

	/* Commit fetch limit */
	private final int mCommitFetchLimit;

	/**
	 * Constructor - takes the context to allow the database to be
	 * opened/created.
	 * 
	 * @param ctx
	 *            the Context within which to work
	 * @param count
	 *            commit fetch limit
	 */
	public RssHandler(Context ctx, int count) {
		this.mCommitDb = new CommitDbAdapter(ctx);
		this.mCommitFetchLimit = count;
		this.mCurState = RssState.CHANNEL;
	};

	@Override
	public void startDocument() throws SAXException {
		mCurState = RssState.CHANNEL;
		mCommitList.clear();
	}

	/**
	 * Process end of an XML element by updating the current state.
	 */
	@Override
	public void startElement(String uri, String name, String qName,
			Attributes atts) {
		String trimmedName = qName.trim();

		switch (mCurState) {
		case CHANNEL:
			if (trimmedName.equals("item")) {
				mCurState = RssState.ITEM;
				mCurCommit = new Commit();
			}
			break;
		case ITEM:
			mRssData = new StringBuilder();
			if (trimmedName.equals("title")) {
				mCurState = RssState.ITEM_TITLE;
			} else if (trimmedName.equals("author")) {
				mCurState = RssState.ITEM_AUTHOR;
			} else if (trimmedName.equals("pubDate")) {
				mCurState = RssState.ITEM_DATE;
			} else if (trimmedName.equals("link")) {
				mCurState = RssState.ITEM_LINK;
			} else if (trimmedName.equals("content:encoded")) {
				mCurState = RssState.ITEM_CONTENT;
			}
			break;
		default:
			/* Uninteresting element, do nothing */
		}
	}

	/**
	 * Process end of an XML element by updating the current state. Insert a
	 * commit into the database if all necessary information has been gathered.
	 */
	@Override
	public void endElement(String uri, String name, String qName)
			throws SAXException {
		String trimmedName = qName.trim();

		switch (mCurState) {
		case ITEM_TITLE:
			if (trimmedName.equals("title")) {
				mCurCommit.mTitle = mRssData.toString();
				mCurState = RssState.ITEM;
			} else {
				throw new SAXParseException(
						"Expected '</title>' instead of '</" + trimmedName
								+ ">'", null);
			}
			break;
		case ITEM_AUTHOR:
			if (trimmedName.equals("author")) {
				final String fullAuthor = mRssData.toString();
				final int ltIdx = fullAuthor.indexOf("<");
				final int gtIdx = fullAuthor.indexOf(">");

				if (ltIdx < gtIdx) {
					mCurCommit.mAuthor = fullAuthor.substring(0, ltIdx).trim();
					mCurCommit.mEmail = fullAuthor.substring(ltIdx + 1, gtIdx)
							.trim();
					mCurState = RssState.ITEM;
				} else {
					throw new SAXParseException("Invalid author field '"
							+ fullAuthor + "'", null);
				}
			} else {
				throw new SAXParseException(
						"Expected '</author>' instead of '</" + trimmedName
								+ ">'", null);
			}
			break;
		case ITEM_DATE:
			if (trimmedName.equals("pubDate")) {
				mCurCommit.mDate = mRssData.toString();
				mCurState = RssState.ITEM;
			} else {
				throw new SAXParseException(
						"Expected '</pubDate>' instead of '</" + trimmedName
								+ ">'", null);
			}
			break;
		case ITEM_LINK:
			if (trimmedName.equals("link")) {
				mCurCommit.mDiffLink = mRssData.toString();
				try {
					mCurCommit.mCommitId = extractCommitId(mCurCommit.mDiffLink);
				} catch (MalformedURLException e) {
					Log.e(TAG, "Error extracting commit ID from URL '"
							+ mCurCommit.mDiffLink + "'");
					throw new SAXException();
				}
				if (mCurCommit.mCommitId.equalsIgnoreCase(mLastCommitId)) {
					throw new CommitMatchException();
				}
				mCurState = RssState.ITEM;
			} else {
				throw new SAXParseException("Expected '</link>' instead of '</"
						+ trimmedName + ">'", null);
			}
			break;
		case ITEM_CONTENT:
			if (trimmedName.equals("content:encoded")) {
				mCurCommit.mContent = mRssData.toString();
				mCurState = RssState.ITEM;
			} else {
				throw new SAXParseException(
						"Expected '</content:encoded>' instead of '</"
								+ trimmedName + ">'", null);
			}
			break;
		case ITEM:
			if (trimmedName.equals("item")) {
				/*
				 * Check if we have collected all necessary information of a
				 * commit
				 */
				if (mCurCommit.isSet()) {
					mCurCommit.trim();
					mCommitList.add(mCurCommit);
					mCurCommit = new Commit();

					if (mCommitList.size() >= mCommitFetchLimit) {
						throw new CommitLimitException();
					}
					mCurState = RssState.CHANNEL;
				} else {
					throw new SAXParseException("Incomplete item", null);
				}
			}
			break;
		default:
			/* Uninteresting element, do nothing */
		}
	}

	/**
	 * Read XML element string and append it to currently processed XML element
	 * data if in correct state.
	 */
	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException {
		switch (mCurState) {
		case ITEM_TITLE:
		case ITEM_AUTHOR:
		case ITEM_DATE:
		case ITEM_LINK:
		case ITEM_CONTENT:
			String chars = new String(ch).substring(start, start + length);
			mRssData.append(chars);
			break;
		}
	}

	/**
	 * Fetch the latest commits from the given feed URL and insert the commits
	 * into the commit database.
	 * 
	 * @param feed
	 *            URL of the gitweb RSS feed
	 * @return number of new commits added to the database
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 */
	public int updateCommits(URL feed) throws ParserConfigurationException,
			SAXException, IOException {
		int commitCount = -1;
		try {
			mCommitDb.open();
			Log.i(TAG, "Updating commits for " + feed.toString());

			updateLastCommitId();
			if (mLastCommitId == "") {
				Log.d(TAG, "No known commits");
			} else {
				Log.d(TAG,
						"Last known commit has ID '"
								+ mLastCommitId.substring(0,
										COMMIT_ABBREV_LENGTH) + "...'");
			}

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			xr.setContentHandler(this);
			xr.parse(new InputSource(feed.openStream()));
			commitCount = flushCommitList();
		} catch (CommitLimitException e) {
			Log.d(TAG,
					"Reached max commit count: "
							+ Integer.toString(mCommitList.size()));
			commitCount = flushCommitList();
		} catch (CommitMatchException e) {
			Log.d(TAG,
					"Commit with same ID ("
							+ mLastCommitId.substring(0, COMMIT_ABBREV_LENGTH)
							+ "...) already present");
			commitCount = flushCommitList();
		} finally {
			mCommitDb.close();
		}
		Log.i(TAG, "Added " + Integer.toString(commitCount) + " commit(s)");
		return commitCount;
	}

	/**
	 * Extracts the commit ID (SHA1 hash) from a commit diff link, e.g. the link
	 * http://git.c[...];a=commitdiff;h=7922ddfd4f3b70df4a8b20c39a2a27cbd709abaf
	 * would return 7922ddfd4f3b70df4a8b20c39a2a27cbd709abaf. Only rudimentary
	 * checks on the input string are performed.
	 * 
	 * @param link
	 *            gitweb commitdiff URL
	 * @return SHA1 commit ID as string
	 * @throws MalformedURLException
	 */
	private String extractCommitId(String link) throws MalformedURLException {
		int curIdx = link.indexOf(COMMIT_DIFF_TAG);
		if (curIdx < 0) {
			Log.d(TAG, "Commit diff URL does not contain " + COMMIT_DIFF_TAG);
			throw new MalformedURLException();
		}

		/* Commit hash following the last '=' must be 40 chars long */
		curIdx = link.lastIndexOf("=");
		if ((link.length() - 1) - curIdx != COMMIT_ID_LENGTH) {
			throw new MalformedURLException();
		}

		return link.substring(curIdx + 1);
	}

	/**
	 * Fetch the last commit id from the database and set the corresponding
	 * member variable accordingly.
	 */
	private void updateLastCommitId() {
		Cursor c = mCommitDb.getLatestCommit();
		if (c.getCount() == 1) {
			mLastCommitId = c.getString(c
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_HASH));
		} else {
			mLastCommitId = "";
		}
		c.close();
	}

	/**
	 * Write all commit objects in the commit list to the database. The list
	 * will be inserted into the database in reverse, meaning the last element
	 * in the commit list will be inserted first.
	 * 
	 * @return number of flushed commits
	 */
	private int flushCommitList() {
		int count = mCommitList.size();
		if (count > 0) {
			ListIterator<Commit> iter = mCommitList.listIterator(mCommitList
					.size());

			while (iter.hasPrevious()) {
				Commit c = iter.previous();
				try {
					mDateFormatter.parse(c.mDate);
					mCommitDb.insertCommit(c);
					Log.d(TAG,
							"Inserted commit with ID '"
									+ c.mCommitId.substring(0,
											COMMIT_ABBREV_LENGTH) + "...'");
				} catch (ParseException e) {
					Log.e(TAG, "Invalid commit date: '" + c.mDate + "'");
				}
			}
		}
		return count;
	}
}
