/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class RssUpdateService extends Service {

	/* TAG for logging purposes */
	public static final String TAG = RssUpdateService.class.getSimpleName();

	/* Arbitrary but unique ID to reference notifications sent by this service */
	private static final int NOTIFICATION_ID = 1439213;

	/* Activity foreground states */
	public enum FgActivity {
		GITTER_COMMITLIST, GITTER_OTHER, OTHER
	};

	/* Notification manager used to notify user about new commits */
	private NotificationManager mNotificationManager;

	/* RSS updater performing the update operation */
	private RssUpdater mRssUpdater;

	/* Timer used to schedule task in regular intervals */
	private Timer mTimer;

	/* Flag specifying if service is performing an user requested update */
	private AtomicBoolean mUserRequest;

	/* Task doing the actual work when the timer fires */
	private UpdateTask mUpdateTask;

	private class UpdateTask extends TimerTask {
		@Override
		public void run() {
			Log.v(TAG, "Timer task running");
			String errorMsg = "";
			int commitCount = -1;
			try {
				commitCount = mRssUpdater.refresh();
			} catch (Exception e) {
				Log.e(TAG, "Error refreshing commits", e);
				errorMsg = e.toString();
			}

			if (commitCount > 0) {
				switch (activityInForeground()) {
				case GITTER_COMMITLIST:
					if (!mUserRequest.get()) {
						notifyCommitListActivity(commitCount, errorMsg);
					}
					break;
				case GITTER_OTHER:
					break;
				default:
					if (notifyUserEnabled()) {
						notifyUser(commitCount);
					}
				}
			}
			if (mUserRequest.getAndSet(false)) {
				notifyCommitListActivity(commitCount, errorMsg);
			}
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "Creating service");

		mTimer = new Timer("RssUpdateTimer");
		mUserRequest = new AtomicBoolean(false);

		mRssUpdater = new RssUpdater(getApplicationContext());
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Destroying service");

		mNotificationManager.cancel(NOTIFICATION_ID);

		mTimer.cancel();
		mTimer = null;
		mUpdateTask = null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		final boolean booting = intent.getBooleanExtra(
				getString(R.string.intent_extra_onboot), false);
		if (booting && !startOnBootEnabled()) {
			Log.i(TAG, "Service start on boot disabled, exiting");
			stopSelf();
			return START_NOT_STICKY;
		}

		mUserRequest.set(!booting);

		Log.d(TAG, "Performing update, reseting timer");
		restartTimer();
		return START_STICKY;
	}

	/**
	 * Restart timer. If it has been started before the update task will be
	 * canceled.
	 */
	protected void restartTimer() {
		if (mUpdateTask != null) {
			mUpdateTask.cancel();
		}
		mUpdateTask = new UpdateTask();
		mTimer.schedule(mUpdateTask, 1000L, updateInterval() * 1000L);
	}

	/**
	 * This function determines what kind of activity is currently in the
	 * foreground.
	 * 
	 * @return FgActivity enum type designating the current foreground activity.
	 */
	private FgActivity activityInForeground() {
		ActivityManager am = (ActivityManager) this
				.getSystemService(ACTIVITY_SERVICE);

		List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
		ComponentName componentInfo = taskInfo.get(0).topActivity;
		final String className = componentInfo.getClassName();

		if (className.startsWith(getPackageName())) {
			if (className.equals(CommitListActivity.class.getName())) {
				return FgActivity.GITTER_COMMITLIST;
			} else {
				return FgActivity.GITTER_OTHER;
			}
		} else {
			return FgActivity.OTHER;
		}
	}

	/**
	 * Returns the update interval in seconds.
	 * 
	 * @return number of seconds between commit updates
	 */
	private int updateInterval() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return Integer.parseInt(prefs.getString(
				getString(R.string.pref_service_update_interval),
				getString(R.string.service_update_interval_default)));
	}

	/**
	 * Returns if True if start of the background service on boot is enabled.
	 * 
	 * @return True if service start on boot is enabled
	 */
	private boolean startOnBootEnabled() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs
				.getBoolean(
						getString(R.string.pref_service_start_on_boot),
						getResources().getBoolean(
								R.bool.service_start_on_boot_default));
	}

	/**
	 * Returns if True if user status bar notifications are enabled.
	 * 
	 * @return True if user notification is enabled
	 */
	private boolean notifyUserEnabled() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(getString(R.string.pref_service_notify_user),
				getResources().getBoolean(R.bool.service_notify_user_default));
	}

	/**
	 * Returns if True if LED notification is enabled.
	 * 
	 * @return True if LED notification is enabled
	 */
	private boolean notifyLedEnabled() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(getString(R.string.pref_service_notify_led),
				getResources().getBoolean(R.bool.service_notify_led_default));
	}

	/**
	 * Returns if True if vibration notification is enabled.
	 * 
	 * @return True if vibration notification is enabled
	 */
	private boolean notifyVibraEnabled() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(getString(R.string.pref_service_notify_vibra),
				getResources().getBoolean(R.bool.service_notify_vibra_default));
	}

	/**
	 * Notify user about new commits by sending a notification to the status
	 * bar.
	 * 
	 * @param count
	 *            number of new commits
	 */
	private void notifyUser(int count) {
		Log.d(TAG, "Notifying user");

		CharSequence text = Integer.toString(count) + " "
				+ getText(R.string.new_commits);

		Notification notification = new Notification(
				R.drawable.ic_stat_notify_commits, text,
				System.currentTimeMillis());
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.number = count;
		if (notifyLedEnabled()) {
			notification.defaults |= Notification.DEFAULT_LIGHTS;
			notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		}

		if (notifyVibraEnabled()) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		}

		Intent notifyIntent = new Intent(this, CommitListActivity.class);
		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notifyIntent, 0);

		notification.setLatestEventInfo(this, getText(R.string.app_name), text,
				contentIntent);

		mNotificationManager.notify(NOTIFICATION_ID, notification);
	}

	/**
	 * Notify commit list activity about new commits.
	 * 
	 * @param count
	 *            number of new commits
	 * @param erroMsg
	 *            optional error message
	 */
	private void notifyCommitListActivity(int count, String errorMsg) {
		Log.d(TAG, "Notifying " + CommitListActivity.class.getName());

		Intent notifyIntent = new Intent(this, CommitListActivity.class);
		notifyIntent.putExtra(getString(R.string.intent_refresh_done),
				mUserRequest.get());
		notifyIntent.putExtra(getString(R.string.intent_commit_count), count);

		if (errorMsg != null) {
			notifyIntent.putExtra(getString(R.string.intent_error_message),
					errorMsg);
		}
		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(notifyIntent);
	}
}
