/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Adapter to the commits database.
 */
public class CommitDbAdapter {

	public static final String KEY_ROWID = "_id";
	public static final String KEY_HASH = "hash";
	public static final String KEY_READ = "read";
	public static final String KEY_TITLE = "title";
	public static final String KEY_AUTHOR = "author";
	public static final String KEY_DATE = "date";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_DIFFLINK = "difflink";
	public static final String KEY_CONTENT = "content";

	/**
	 * Read status value to mark as read
	 */
	public static final int READ_VALUE = 1;

	private static final String TAG = CommitDbAdapter.class.getSimpleName();
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	/**
	 * Database creation SQL statement.
	 */
	private static final String DATABASE_CREATE = "create table commits ("
			+ KEY_ROWID + " integer primary key autoincrement," + KEY_HASH
			+ " varchar(40) not null," + KEY_READ + " integer default 0,"
			+ KEY_TITLE + " text not null," + KEY_AUTHOR + " text not null,"
			+ KEY_DATE + " text not null," + KEY_EMAIL + " text not null,"
			+ KEY_DIFFLINK + " text not null," + KEY_CONTENT
			+ " text not null)";
	private static final String DATABASE_NAME = "data";
	private static final String DATABASE_TABLE = "commits";

	private static final int DATABASE_VERSION = 5;

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS commits");
			onCreate(db);
		}
	}

	/**
	 * Constructor - takes the context to allow the database to be
	 * opened/created.
	 * 
	 * @param ctx
	 *            the Context within which to work
	 */
	public CommitDbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	/**
	 * Close the connection.
	 */
	public void close() {
		mDbHelper.close();
	}

	/**
	 * Open the commits database. If it cannot be opened, try to create a new
	 * instance of the database. If it cannot be created, throw an exception to
	 * signal the failure.
	 * 
	 * @return this (self reference, allowing this to be chained in an
	 *         initialization call)
	 * @throws SQLException
	 *             if the database could be neither opened or created
	 */
	public CommitDbAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	/**
	 * Return a Cursor over the list of all commits in the database.
	 * 
	 * @return Cursor over all commits
	 */
	public Cursor fetchAllCommits() {
		return mDb.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_HASH,
				KEY_READ, KEY_TITLE, KEY_AUTHOR, KEY_DATE, KEY_EMAIL,
				KEY_DIFFLINK, KEY_CONTENT }, null, null, null, null, KEY_ROWID
				+ " desc");
	}

	/**
	 * Insert a new commit into the commit DB. If the commit is successfully
	 * created return the new rowId for that commit, otherwise return -1 to
	 * indicate failure.
	 * 
	 * @param newCommit
	 *            the commit to insert into the DB
	 * @return rowId or -1 if failed
	 */
	public long insertCommit(Commit newCommit) {
		final ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_HASH, newCommit.mCommitId);
		initialValues.put(KEY_TITLE, newCommit.mTitle);
		initialValues.put(KEY_AUTHOR, newCommit.mAuthor);
		initialValues.put(KEY_DATE, newCommit.mDate);
		initialValues.put(KEY_EMAIL, newCommit.mEmail);
		initialValues.put(KEY_DIFFLINK, newCommit.mDiffLink);
		initialValues.put(KEY_CONTENT, newCommit.mContent);

		return mDb.insert(DATABASE_TABLE, null, initialValues);
	}

	/**
	 * Delete commit with given rowId.
	 * 
	 * @param rowId
	 *            id of commit to delete
	 * @return true if deleted, false otherwise
	 */
	public boolean deleteCommit(long rowId) {
		return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	/**
	 * Return a Cursor positioned at the commit that matches the given rowId.
	 * 
	 * @param rowId
	 *            id of commit to retrieve
	 * @return Cursor positioned to matching commit, if found
	 * @throws SQLException
	 *             if commit could not be found/retrieved
	 */
	public Cursor fetchCommit(long rowId) throws SQLException {

		final Cursor c = mDb.query(true, DATABASE_TABLE, new String[] {
				KEY_ROWID, KEY_HASH, KEY_READ, KEY_AUTHOR, KEY_DATE, KEY_EMAIL,
				KEY_TITLE, KEY_CONTENT, KEY_DIFFLINK },
				KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}

	/**
	 * Return a Cursor positioned at the latest commit in the database.
	 * 
	 * @return Cursor positioned to latest commit, if found
	 * @throws SQLException
	 *             if commit could not be found/retrieved
	 */
	public Cursor getLatestCommit() throws SQLException {

		final Cursor c = mDb.query(true, DATABASE_TABLE, new String[] {
				KEY_ROWID, KEY_HASH, KEY_READ, KEY_AUTHOR, KEY_EMAIL,
				KEY_TITLE, KEY_CONTENT, KEY_DIFFLINK }, null, null, null, null,
				KEY_ROWID + " desc", "1");
		if (c != null) {
			c.moveToFirst();
		}

		return c;
	}

	/**
	 * Set 'read' status of commit identified by rowId to given status.
	 * 
	 * @param rowId
	 *            id of commit to update
	 * @param read
	 *            status to set commit to (true or false)
	 * @return true if the commit was successfully updated, false otherwise
	 */
	public boolean setReadStatus(long rowId, boolean read) {
		final ContentValues args = new ContentValues();
		args.put(KEY_READ, read);
		Log.d(TAG, "Read status of row with id " + rowId + " set to " + read);
		return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	/**
	 * Clear commit database.
	 * 
	 * @return number of deleted commits
	 */
	public int clearDatabase() {
		final int rows = mDb.delete(DATABASE_TABLE, "1", null);
		Log.d(TAG, "Deleted " + rows + " commit(s) from database");
		return rows;
	}

	/**
	 * Return a Cursor over the list of all unread commits in the database.
	 * 
	 * @return Cursor over all unread commits
	 */
	public Cursor fetchUnreadCommits() {
		return mDb.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_HASH,
				KEY_READ, KEY_TITLE, KEY_AUTHOR, KEY_DATE, KEY_EMAIL,
				KEY_DIFFLINK, KEY_CONTENT }, null, null, KEY_ROWID, KEY_READ
				+ "= 0", KEY_ROWID + " desc");
	}

	/**
	 * Ensures that all commits are set to read status READ_VALUE
	 * 
	 * @return number of commits needed to set to read
	 */
	public int markAllAsRead() {
		final ContentValues args = new ContentValues();
		args.put(KEY_READ, READ_VALUE);
		Log.d(TAG, "Set read status of all commits to " + READ_VALUE);
		return mDb.update(DATABASE_TABLE, args, KEY_READ + "= 0", null);
	}
}
