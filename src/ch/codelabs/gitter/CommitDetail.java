/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

public class CommitDetail extends Activity {

	private CommitDbAdapter mDbAdapter;
	private Long mRowId;
	private TextView mAuthorText;
	private TextView mTitleText;
	private TextView mHashText;
	private TextView mDateText;
	private WebView mContent;
	private String mDiffURL;
	private String mEmail;
	private String mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDbAdapter = new CommitDbAdapter(this);
		mDbAdapter.open();

		setContentView(R.layout.commit_detail);

		mAuthorText = (TextView) findViewById(R.id.commit_author);
		mTitleText = (TextView) findViewById(R.id.commit_title);
		mHashText = (TextView) findViewById(R.id.commit_hash);
		mDateText = (TextView) findViewById(R.id.commit_date);

		mContent = (WebView) findViewById(R.id.commit_content);
		mContent.getSettings().setBuiltInZoomControls(true);

		mRowId = (savedInstanceState == null) ? null
				: (Long) savedInstanceState
						.getSerializable(CommitDbAdapter.KEY_ROWID);
		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			mRowId = extras != null ? extras.getLong(CommitDbAdapter.KEY_ROWID)
					: null;
		}

		populateFields();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mDbAdapter.close();
	}

	@Override
	protected void onResume() {
		super.onResume();
		populateFields();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(CommitDbAdapter.KEY_ROWID, mRowId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.detail_options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_open_diff:
			doDiffViewIntent();
			return true;
		case R.id.menu_send_mail:
			doSendMail();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void populateFields() {
		if (mRowId != null) {
			Cursor commit = mDbAdapter.fetchCommit(mRowId);
			startManagingCursor(commit);

			mDiffURL = commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_DIFFLINK));
			mEmail = commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_EMAIL));
			mTitle = commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_TITLE));

			mTitleText.setText(mTitle);
			mAuthorText.setText(commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_AUTHOR)));
			mDateText.setText(commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_DATE)));
			mHashText.setText(commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_HASH)));

			/*
			 * Replace newlines with <br> between <pre></pre> tags. TODO:
			 * Improve replacement code, use regex e.g.
			 */
			final String content = commit.getString(commit
					.getColumnIndexOrThrow(CommitDbAdapter.KEY_CONTENT));
			final int preStart = content.indexOf("<pre>");
			final int preStop = content.lastIndexOf("</pre>");
			final String htmlData = "<html><body>"
					+ content.substring(preStart, preStop)
							.replace("\n", "<br>")
					+ content.substring(preStop + 6) + "</body></html>";
			mContent.loadData(htmlData, "text/html", null);
		}
	}

	/**
	 * Send view intent with diff URL.
	 */
	protected void doDiffViewIntent() {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(mDiffURL));
		startActivity(browserIntent);
	}

	/**
	 * Send feedback e-mail to author.
	 */
	protected void doSendMail() {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { mEmail });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mTitle);

		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "-- "
				+ "\nSent by gitter\nCommit-Diff:" + mDiffURL);

		emailIntent.setType("message/rfc822");
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	}
}
