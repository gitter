/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import java.io.InputStream;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CommitListActivity extends ListActivity {

	private static final int COMMIT_DETAIL = 0;
	private static final int PREFS_UPDATED = 1;
	static final int PROGRESS_DIALOG = 2;
	static final int ABOUT_DIALOG = 3;

	private CommitDbAdapter mDbAdapter;

	/* The activity's state */
	private ActivityState mState;

	/**
	 * Holds a reference to the current row's image view to optimize access to
	 * the view. Calling findViewById is a costly operation which can be avoided
	 * by getting the image view once and attaching a view holder instance as
	 * tag to the commit row.
	 */
	private static class ViewHolder {
		public ImageView mImage;
	}

	private class CommitEntryCursorAdapter extends SimpleCursorAdapter {

		public CommitEntryCursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View commitRow = super.getView(position, convertView, parent);
			Cursor c = getCursor();
			c.moveToPosition(position);
			int col = c.getColumnIndex(CommitDbAdapter.KEY_READ);
			boolean isRead = c.getInt(col) == 1;
			final int bgColor = (isRead) ? R.drawable.commitrow_read_bg
					: R.drawable.commitrow_unread_bg;
			commitRow.setBackgroundResource(bgColor);

			if (commitRow.getTag() == null) {
				ViewHolder viewHolder = new ViewHolder();
				viewHolder.mImage = (ImageView) commitRow
						.findViewById(R.id.contact_picture);
				commitRow.setTag(viewHolder);
			}
			ViewHolder vh = (ViewHolder) commitRow.getTag();
			if (showContactPhotos() && vh.mImage != null) {
				Bitmap pic = getContactPhoto(c.getString(c
						.getColumnIndex(CommitDbAdapter.KEY_AUTHOR)));
				vh.mImage.setImageBitmap(pic);
			}
			return commitRow;
		}
	}

	/* Flag signaling if progress bar is showing */
	private boolean mProgressDialog;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(getString(R.string.app_name), "Commit List created");
		setContentView(R.layout.commit_list);

		Object data = getLastNonConfigurationInstance();
		if (data instanceof ActivityState) {
			mState = (ActivityState) data;
			if (mState.mRssUpdater != null) {
				Log.d(getString(R.string.app_name),
						"Reclaiming previous RSS update task.");
				mState.mRssUpdater.setActivity(this);
			}
		} else {
			mState = new ActivityState();
		}

		mDbAdapter = new CommitDbAdapter(this);
		mDbAdapter.open();
		fillData();
	}

	@Override
	protected void onDestroy() {
		mDbAdapter.close();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			doRefresh();
			return true;
		case R.id.menu_stop_service:
			doStopService();
			return true;
		case R.id.menu_settings:
			Intent i = new Intent(this, PrefsActivity.class);
			startActivityForResult(i, PREFS_UPDATED);
			return true;
		case R.id.menu_about:
			showDialog(ABOUT_DIALOG);
			return true;
		case R.id.menu_delete:
			doClear();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// set read status to read
		mDbAdapter.setReadStatus(id, Boolean.TRUE);
		Intent i = new Intent(this, CommitDetail.class);
		i.putExtra(CommitDbAdapter.KEY_ROWID, id);
		startActivityForResult(i, COMMIT_DETAIL);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		switch (requestCode) {
		case COMMIT_DETAIL:
			fillData();
			break;
		case PREFS_UPDATED:
			if (intent != null) {
				handlePrefChanges(intent);
			}
			break;
		default:
		}
	}

	/**
	 * Retain activity state.
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		if (mState.mRssUpdater != null
				&& mState.mRssUpdater.getStatus() != AsyncTask.Status.RUNNING) {
			/* Do not retain updater if it is not actually running */
			mState.mRssUpdater = null;
		}
		return mState;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case PROGRESS_DIALOG:
			ProgressDialog dialog = new ProgressDialog(this);
			dialog.setMessage(getString(R.string.refresh_message));
			dialog.setIndeterminate(true);
			dialog.setOnDismissListener(new OnDismissListener() {

				/**
				 * Reset progress dialog flag if it is dismissed.
				 */
				public void onDismiss(DialogInterface dialog) {
					mProgressDialog = false;
				}
			});
			return dialog;

		case ABOUT_DIALOG:
			LayoutInflater li = LayoutInflater.from(this);
			View view = li.inflate(R.layout.about, null);

			TextView version_tv = (TextView) view
					.findViewById(R.id.about_version_tv);
			version_tv.setText(getVersionNumber());

			TextView author_tv = (TextView) view
					.findViewById(R.id.about_authors_tv);
			author_tv.setText(getAuthors());
			return new AlertDialog.Builder(this)
					.setTitle(getString(R.string.menu_about)).setView(view)
					.create();

		default:
			return super.onCreateDialog(id);
		}
	}

	/**
	 * Set dialog flag for the one that is shown.
	 */
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		if (id == PROGRESS_DIALOG) {
			mProgressDialog = true;
		}
	}

	/**
	 * Update commits using RSS handler.
	 */
	protected void doRefresh() {
		if (serviceEnabled()) {
			Log.d(getString(R.string.app_name),
					"Refreshing commits by sending request to "
							+ RssUpdateService.TAG);
			Intent i = new Intent(this, RssUpdateService.class);
			startService(i);
			mState.mServiceRunning = true;
		} else {
			Log.d(getString(R.string.app_name),
					"Refreshing commits using async task");
			if (mState.mRssUpdater != null
					&& mState.mRssUpdater.getStatus() == AsyncTask.Status.RUNNING) {
				Log.i(getString(R.string.app_name),
						"Update already in progress, nothing to do");
				return;
			}
			mState.mRssUpdater = new RssUpdateTask(this);
			mState.mRssUpdater.execute();
		}
		showDialog(PROGRESS_DIALOG);
	}

	/**
	 * Stop RSS update service running in background.
	 */
	protected void doStopService() {
		Log.d(getString(R.string.app_name), "Stopping " + RssUpdateService.TAG);
		Intent i = new Intent(this, RssUpdateService.class);
		stopService(i);
		mState.mServiceRunning = false;
		Toast.makeText(this, getString(R.string.service_stopped),
				Toast.LENGTH_SHORT).show();
	}

	/**
	 * Clear commit DB.
	 */
	protected void doClear() {
		// TODO: check if database is empty, if yes return
		final int rowsDeleted = mDbAdapter.clearDatabase();
		fillData();
		Toast.makeText(this,
				rowsDeleted + " " + getString(R.string.deleted_commits),
				Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		onUpdateCompleted(
				intent.getIntExtra(getString(R.string.intent_commit_count), 0),
				intent.getStringExtra(getString(R.string.intent_error_message)));
	}

	/**
	 * Called if RSS feed update has been completed. Dismiss progress dialog if
	 * showing, show toast message with number of new commits and update data in
	 * listview. The optional error message is appended to the toast message if
	 * specified.
	 * 
	 * @param count
	 *            number of new commits
	 * @param erroMsg
	 *            optional error message
	 */
	public void onUpdateCompleted(int count, String errorMsg) {
		if (mProgressDialog) {
			dismissDialog(PROGRESS_DIALOG);
		}

		String message;
		if (count > 0) {
			message = Integer.toString(count) + " "
					+ getString(R.string.new_commits);
		} else if (count == 0) {
			message = getString(R.string.no_new_commits);
		} else {
			message = getString(R.string.update_error);
			if (errorMsg != null && errorMsg.length() > 0) {
				message = message + System.getProperty("line.separator")
						+ errorMsg;
			}
		}

		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

		fillData();
	}

	private void fillData() {
		// Get all of the commits from the database and create the commit list
		Cursor c = mDbAdapter.fetchAllCommits();
		startManagingCursor(c);

		String[] from = new String[] { CommitDbAdapter.KEY_TITLE,
				CommitDbAdapter.KEY_AUTHOR, CommitDbAdapter.KEY_DATE,
				CommitDbAdapter.KEY_READ };
		int[] to = new int[] { R.id.commit_title_list, R.id.commit_author_list,
				R.id.commit_date_list };

		// Now create an array adapter and set it to display using our row
		SimpleCursorAdapter commits = new CommitEntryCursorAdapter(this,
				R.layout.commits_row, c, from, to);
		setListAdapter(commits);
		Log.i(getString(R.string.app_name), "Commits view synced with database");
	}

	/**
	 * React upon preference changes.
	 * 
	 * @param Intent
	 *            The result intent received from the preferences activity.
	 */
	private void handlePrefChanges(Intent i) {
		if (i.hasExtra(getString(R.string.intent_pref_url_changed))) {
			Log.i(getString(R.string.app_name),
					"Reinitializing commits because of repository URL change");
			mDbAdapter.clearDatabase();
			doRefresh();
		} else if (i.getBooleanExtra(
				getString(R.string.intent_pref_update_interval_changed), false)) {
			doRefresh();
			return;
		} else if (i.getBooleanExtra(
				getString(R.string.intent_pref_service_state_changed), false)) {
			if (serviceEnabled() && !mState.mServiceRunning) {
				doRefresh();
			} else if (!serviceEnabled() && mState.mServiceRunning) {
				doStopService();
			}
		}
	}

	/**
	 * Get current version number.
	 * 
	 * @return String version
	 */
	private String getVersionNumber() {
		String version = "?";
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			Log.d(getString(R.string.app_name),
					"Unable to determine version number: package name not found");
		}

		return version;
	}

	/**
	 * Get list of authors including their email addresses.
	 * 
	 * @return String authors of this application
	 */
	private String getAuthors() {
		StringBuilder authors = new StringBuilder();

		String[] names = getResources().getStringArray(R.array.author_names);
		String[] emails = getResources().getStringArray(R.array.author_emails);
		final int len = names.length;

		if (len < 1) {
			return getString(R.string.app_authors);
		}

		for (int i = 0; i < len; ++i) {
			authors.append(names[i] + " <" + emails[i] + ">"
					+ System.getProperty("line.separator"));
		}

		return authors.toString().trim();
	}

	/**
	 * Returns if True if the background service is enabled.
	 * 
	 * @return True if service is enabled
	 */
	private boolean serviceEnabled() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(getString(R.string.pref_service_enabled),
				getResources().getBoolean(R.bool.service_enabled_default));
	}

	/**
	 * Returns if True if showing contact photos is enabled.
	 * 
	 * @return True if contact photos is enabled
	 */
	private boolean showContactPhotos() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(getString(R.string.pref_show_contact_photo),
				getResources().getBoolean(R.bool.show_contact_photo_default));
	}

	/**
	 * Class enveloping the commit list activity's state.
	 */
	private class ActivityState {
		/*
		 * The activity's RSS asynchronous updater used to when the background
		 * service is disabled by the user.
		 */
		private RssUpdateTask mRssUpdater;

		/* Flag signaling if background service is running */
		private boolean mServiceRunning;

		/* Author to photo map */
		public HashMap<String, Bitmap> mAuthorPhotos;

		ActivityState() {
			mServiceRunning = false;
			mAuthorPhotos = new HashMap<String, Bitmap>();
		}
	}

	/**
	 * Get contact photo for a given name.
	 */
	private Bitmap getContactPhoto(String name) {
		if (mState.mAuthorPhotos.containsKey(name)) {
			return mState.mAuthorPhotos.get(name);
		}

		Uri uri = ContactsContract.Contacts.CONTENT_URI;
		final String[] projection = new String[] { ContactsContract.Contacts._ID };
		final String selection = ContactsContract.Contacts.DISPLAY_NAME
				+ " LIKE ?";
		final String[] selectionArgs = new String[] { name };
		Cursor c = getContentResolver().query(uri, projection, selection,
				selectionArgs, null);

		Bitmap photo = null;
		try {
			if (c.moveToFirst()) {
				do {
					photo = loadContactPhoto(getContentResolver(), c.getLong(c
							.getColumnIndex(ContactsContract.Contacts._ID)));
				} while (c.moveToNext() && photo == null);
			}
		} finally {
			c.close();
		}
		if (photo != null) {
			Log.d(getString(R.string.app_name), "Caching contact photo of "
					+ name);
		} else {
			photo = BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_contact_picture);

		}
		mState.mAuthorPhotos.put(name, photo);
		return photo;
	}

	/**
	 * Return photo of the contact specified by ID if it exists.
	 * 
	 * @param cr
	 *            content resolver
	 * @param id
	 *            contact id
	 * @return contact photo or null if none exists
	 */
	private static Bitmap loadContactPhoto(ContentResolver cr, long id) {
		Uri uri = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, id);
		InputStream input = ContactsContract.Contacts
				.openContactPhotoInputStream(cr, uri);
		if (input == null) {
			return null;
		}
		return BitmapFactory.decodeStream(input);
	}
}
