/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import java.net.URL;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

public class RssUpdater {

	/* TAG for logging purposes */
	public static final String TAG = RssUpdater.class.getSimpleName();

	/* Application context */
	private final Context mContext;

	/* Connectivity manager used to check connection type (WLAN) */
	private final ConnectivityManager mConnectivityManager;

	/* RSS handler used to update commits */
	private final RssHandler mRssHandler;

	/**
	 * Constructor for RSS update task.
	 * 
	 * @param activity
	 *            reference to activity to post progress to
	 */
	public RssUpdater(Context context) {
		mContext = context;
		mRssHandler = new RssHandler(context, commitFetchLimit());
		mConnectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	/**
	 * Refresh commit data from RSS feed.
	 * 
	 * @return number of commits fetched.
	 * @throws Exception
	 * @throws NotFoundException
	 */
	public int refresh() throws NotFoundException, Exception {
		/* Only update if network connection is WLAN */
		NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
		if (netInfo.getType() != ConnectivityManager.TYPE_WIFI && WifiOnly()) {
			Log.d(TAG,
					mContext.getResources().getString(
							R.string.skip_nonwifi_update));
			throw new Exception(mContext.getResources().getString(
					R.string.skip_nonwifi_update));
		} else {
			return mRssHandler.updateCommits(new URL(repoUrl()));
		}
	}

	/**
	 * Returns the URL of the repository RSS feed to fetch commits from.
	 * 
	 * @return URL of repository
	 */
	private String repoUrl() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return prefs
				.getString(
						mContext.getResources().getString(
								R.string.pref_repository_url),
						mContext.getResources().getString(
								R.string.app_repository_rss_url));
	}

	/**
	 * Returns if True if only updates via Wifi are configured by the user.
	 * 
	 * @return True if the user has enabled Wifi-only updates
	 */
	private boolean WifiOnly() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return prefs.getBoolean(
				mContext.getResources().getString(R.string.pref_use_wifi_only),
				mContext.getResources()
						.getBoolean(R.bool.use_wifi_only_default));
	}

	/**
	 * Returns the commit fetch limit configured by the user.
	 * 
	 * @return maximum number of commits to fetch when updating
	 */
	private int commitFetchLimit() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return Integer.parseInt(prefs.getString(mContext.getResources()
				.getString(R.string.pref_commit_fetch_limit), mContext
				.getString(R.string.commit_fetch_limit_default)));
	}
}
