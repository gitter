/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter;

import android.os.AsyncTask;
import android.util.Log;

class RssUpdateTask extends AsyncTask<Void, Void, Integer> {

	/* TAG for logging purposes */
	public static final String TAG = RssUpdateTask.class.getSimpleName();

	/* Reference to activity to handle progress bar */
	private CommitListActivity activity;

	/* RSS updater performing the update operation */
	private final RssUpdater mRssUpdater;

	/* State of update operation */
	private boolean mUpdateDone;

	/* Number of fetched commits */
	private int mCount = -1;

	/* Optional error message */
	private String mErrorMsg;

	/**
	 * Constructor for RSS update task.
	 * 
	 * @param activity
	 *            reference to activity to post progress to
	 */
	public RssUpdateTask(CommitListActivity activity) {
		this.activity = activity;
		mRssUpdater = new RssUpdater(activity);
		mUpdateDone = false;
	}

	/**
	 * Showing progress dialog on the UI thread.
	 */
	@Override
	protected void onPreExecute() {
		activity.showDialog(CommitListActivity.PROGRESS_DIALOG);
	}

	/**
	 * Fetch new commits from RSS feed.
	 */
	@Override
	protected Integer doInBackground(Void... params) {
		int commitCount = -1;
		try {
			commitCount = mRssUpdater.refresh();
		} catch (Exception e) {
			Log.e(TAG, "Error refreshing commits", e);
			mErrorMsg = e.toString();
		}
		return commitCount;
	}

	/**
	 * Notifiy the Activity on completion.
	 */
	@Override
	protected void onPostExecute(Integer result) {
		mUpdateDone = true;
		mCount = result;
		notifyActivity();
	}

	/**
	 * Setter for activity of task. This assures notification of the activity
	 * works even if it gets recreated (e.g. rotation) while the task is
	 * running.
	 * 
	 * @param activity
	 */
	public void setActivity(CommitListActivity activity) {
		this.activity = activity;
		if (mUpdateDone) {
			notifyActivity();
		}
	}

	/**
	 * Helper method to notify the activity that the task has completed.
	 */
	public void notifyActivity() {
		if (activity != null) {
			activity.onUpdateCompleted(mCount, mErrorMsg);
		}
	}
}